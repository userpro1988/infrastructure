# Summary

_Note: For database incidents please use the [database incident template](.gitlab/issue_templates/database_incident.md)_

<!-- Add a brief summary of the incident here and a severity label (S1, S2, S3, S4), then remove this comment. -->

# Timeline of events

<!--

Here you should add the timeline of events, such as when the incident happened,
when action X was taken, etc.

-->

* YYYY-MM-DD XX:YY UTC: action X taken

## Monitoring

<!-- Add links to graphs-->

## Logs

<!-- Add links to logs -->

/label ~outage
