# GitLab Infrastructure Onboarding

## Brew Files

These sample brew files are meant as examples of how different engineers set up
their workstations.  It is probably best to pick and choose from a few different
files, rather than using any one of them as is.

The `production.brewfile` contains useful packages for all production SREs.
